import React from 'react';

function Unread (props) {
  return(
      <span>{props.value}</span>
);}

Unread.propTypes = {
  value: React.PropTypes.string
};

export default Unread;
