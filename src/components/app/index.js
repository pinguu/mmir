import React from 'react';
import Title from '../title';
import User from '../user';

function App () {
    return(
        <div>
            <Title>
                Title of my component 
            </Title>
            <User picture="https://placehold.it/350x150" userName = "Toto" value='9'/>
        </div>
);}

export default App;
