import React from 'react';
import Unread from '../unread';

function User (props) {
  return(
      <div>
          <img src = {props.picture}/>
          <span>{props.userName}</span>
          <Unread value={props.value} />
      </div>
);}


User.propTypes = {
  picture: React.PropTypes.string,
  userName: React.PropTypes.string,
  value: React.PropTypes.string
};

export default User;
