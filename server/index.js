import express from 'express';
import path from 'path';

const port = process.env.PORT || 3000;
const app = express();

//initialize client route
const distPath = path.join(__dirname, '../dist');
const indexFileName = 'index.html';
app.use(express.static(distPath));
app.get('*', () => express.static(path.join(distPath, indexFileName)));

const server = app.listen(port, (err) => {
    if (err) {
      console.log(err);
      return;
    }
    console.log(` 🌎  API is listening on port ${ server.address().port } `);
});
