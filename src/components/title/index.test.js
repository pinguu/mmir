import React from 'react';

import { expect } from 'chai';
import { describe, it } from 'mocha';
import { shallow } from 'enzyme';

import Title from './index';

describe('Title', () => {
  it('should render its children', () => {
     const wrapper = shallow(<Title>Hello world</Title>);
     expect(wrapper.text()).to.equal('Hello world');
   });
});
