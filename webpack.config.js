const path = require('path');
const plugins = require('./webpack/plugins');
function getEntrySources(sources) {
  if (process.env.NODE_ENV === 'development') {
    sources.push('webpack-hot-middleware/client?http://localhost:8080/');
    sources.push('webpack/hot/only-dev-server');
  }
  return sources;
}
module.exports = {
  entry: { app: getEntrySources(['./src/index.js']) },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].[hash].js',
    publicPath: '/',
    sourceMapFilename: '[name].[hash].js.map',
    chunkFilename: '[id].chunk.js',
  },
  devtool: 'source-map',
  plugins: plugins,
  module: {
    preLoaders: [
      { test: /\.js$/, loader: 'source-map-loader' },
      { test: /\.js$/, loader: 'eslint-loader' },
    ],
    loaders: [
      { test: /\.js$/, loaders: ['babel-loader'], exclude: /node_modules/ },
      { test: /\.json$/, loader: 'json-loader' },
      { test: /\.(png|jpg|jpeg|gif|svg)$/, loader: 'url-loader?prefix=img/&limit=5000' },
      { test: /\.(woff|woff2|ttf|eot)$/, loader: 'url-loader?prefix=font/&limit=5000' },
    ],
  },
};
